package co.datainsider

import util.TimeUtils.{firstDateOfMonth, format, formatDate, getDate, getNextDay, getNextMonth, lastDayOfMonth, lastNDays, runWithMockTime, toDateCol, toMonth, today, yesterday}

import co.datainsider.ClickhouseConfiguration.CLICKHOUSE_DATE_PATTERN
import co.datainsider.util.{PathUtils, TableType, TimeUtils}
import co.datainsider.util.PathUtils.{buildPath, buildPaths}
import org.apache.hadoop.shaded.org.jline.utils.Log.error
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import co.datainsider.util.Implicits._

import java.util.Date

case class NewUserConfig(
                          parquetPath: String,
                          columnId: String,
                          destinationHadoopPath: String,
                          destinationClickhouseDatabase: String,
                          destinationClickhouseTable: String
                        )

/**
  * @author tvc12 - Thien Vi
  * @created 12/09/2021 - 1:42 PM
  */
object Main {



  def main(args: Array[String]): Unit = {
    val sparkSession: SparkSession = SparkSession.builder.appName("Daily new user analysis").getOrCreate()
    val processDate = yesterday()
    // /data/db/DWH_STAGING/CASS_USER_REGISTER_V2/YYYY/MM/DD/*
    val newUserDataPath = buildPath("/data/db/DWH_STAGING/CASS_USER_REGISTER_V2", processDate, "*")
    // /data/result/E_WALLET/A1.NEW_USER.parquet
    val destinationHadoopPath = buildPath("/data/result/samples", processDate, "A1.NEW_USER.parquet")
    val destDatabase = "SAMPLE_DATABASE"
    val destTableName = "DAILY_USER_REGISTER"
    processNewUser(sparkSession, yesterday(), NewUserConfig(newUserDataPath, "USER_ID", destinationHadoopPath, destDatabase, destTableName))
  }


  def processNewUser(sparkSession: SparkSession, processData: Date, newUserConfig: NewUserConfig): Unit = {
    // create temp view
    sparkSession.read.parquet(newUserConfig.parquetPath).createOrReplaceTempView("NEW_USER")
    // select from view
    val df: DataFrame = sparkSession.sql(s"""
        |SELECT DISTINCT ${newUserConfig.columnId} as USER_ID
        |FROM NEW_USER
        |""".stripMargin
    )
    df.saveToHadoop(newUserConfig.destinationHadoopPath)

    val numberNewUser: Long = df.count()
    val formattedDate = format(processData, CLICKHOUSE_DATE_PATTERN)
    val numberWalletDf: DataFrame = sparkSession.sql(
      s"""
       |SELECT
       | to_date('${formattedDate}') as DATE,
       | $numberNewUser as NUMBER_USERS
       | """.stripMargin)
    numberWalletDf.saveToClickHouse(newUserConfig.destinationClickhouseDatabase, newUserConfig.destinationClickhouseTable, SaveMode.Append)
  }
}
