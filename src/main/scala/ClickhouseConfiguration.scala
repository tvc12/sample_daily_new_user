package co.datainsider

import java.util.Properties

/**
  * @author tvc12 - Thien Vi
  * @created 12/07/2021 - 7:40 PM
  */
object ClickhouseConfiguration {
  val DI_HOST = "http://172.32.1.102:8489/ingestion"
  val CLICKHOUSE_URL = "jdbc:clickhouse://172.32.1.33:9000"
  val DEFAULT_WRITE_MODE = "append"
  val SECRET_KEY = "12345678"
  val DATE_PATTERN = "yyyy/MM/dd"
  val CLICKHOUSE_DATE_PATTERN = "yyyy-MM-dd"
  val TIMEOUT_IN_SECOND = 60
  val USER = "default"
  val PASSWORD = ""

  def JDBC_PROPERTIES: Properties = {
    val driver = "com.github.housepower.jdbc.ClickHouseDriver"

    val properties = new Properties
    properties.setProperty("user", USER)
    properties.setProperty("password", PASSWORD)
    properties.setProperty("driver", driver)
    properties
  }
}
