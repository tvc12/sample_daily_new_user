package co.datainsider
package util

import org.apache.hadoop.shaded.org.jline.utils.Log.info

import java.nio.file.Paths
import java.util.Date
import scala.collection.mutable.ArrayBuffer

object PathUtils {
  private val DATE_PATTERN = "yyyy/MM/dd"

  def getPartitionPathByDate(date: Date): String = {
    TimeUtils.format(date, DATE_PATTERN)
  }

  def buildPath(baseDir: String,
                partitionDate: Date,
                subFolders: String*): String = {
    val partitionPath: String = PathUtils.getPartitionPathByDate(partitionDate)
    val path: String = Paths.get(baseDir, (Seq(partitionPath) ++: subFolders.toSeq): _*).toString
    info(s"buildPath:: ${path}")
    path
  }

  /**
    * Build path from [StartDate, EndDate]
    * @param baseDir: base dir of path
    * @param startDate: start date for partition
    * @param endDate: end date for partition
    * @param subFolders: sub folder name
    * @return [base/partition_start/sub/folder, ... base/partition_end/sub/folder]
    */
  def buildPaths(baseDir: String,
                 startDate: Date,
                 endDate: Date,
                 subFolders: String*): Array[String] = {
    var currentDate: Date = startDate
    val paths = ArrayBuffer[String]()
    while (currentDate.before(endDate)) {
      val path = buildPath(baseDir, currentDate, subFolders: _*)
      paths.append(path)
      currentDate = TimeUtils.getNextDay(currentDate)
    }
    val endPath = buildPath(baseDir, currentDate, subFolders: _*)
    paths.append(endPath)
    info(s"buildPaths:: ${paths.toString()}")

    paths.toArray
  }

}
