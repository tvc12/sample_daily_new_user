package co.datainsider
package util

import co.datainsider.HadoopConfiguration.TRASH_PATH
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext

/**
  * @author tvc12 - Thien Vi
  * @created 12/15/2021 - 11:40 PM
  */
object HadoopUtils {

  def isPathExists(sparkContext: SparkContext, path: String): Boolean = {
    val fs =
      org.apache.hadoop.fs.FileSystem.get(sparkContext.hadoopConfiguration)
    val newPath = path.replaceAll("/\\*\\s*$", "")
    fs.exists(new org.apache.hadoop.fs.Path(newPath))
  }

  def includeCurrentTimestamp(name: String): String = {
    s"${name}_${System.currentTimeMillis()}"
  }

  def moveToTrash(sparkContext: SparkContext, path: String): Boolean = {
    val fs = FileSystem.get(sparkContext.hadoopConfiguration)
    val oldPath = new Path(path)
    val destPath = new Path(TRASH_PATH, includeCurrentTimestamp(oldPath.getName))
    fs.mkdirs(new Path(TRASH_PATH))
    fs.rename(oldPath, destPath)
  }
}
