package co.datainsider
package util

import co.datainsider.ClickhouseConfiguration.DATE_PATTERN
import org.apache.hadoop.shaded.org.jline.utils.Log.error
import org.apache.spark.sql.Column
import org.apache.spark.sql.functions.from_unixtime

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

/**
  * @author tvc12 - Thien Vi
  * @created 12/09/2021 - 2:09 PM
  */
object TimeUtils {
  def getDate(year: Int, month: Int, date: Int): Date = {
    val calendar = Calendar.getInstance
    clearTime(calendar)
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, date)
    calendar.getTime
  }

  private var mockToday: Date = null
  // METHOD for testing
  def setToDay(date: Date): Unit = {
    mockToday = date
  }

  def getNextDay(currentDate: Date): Date = {
    val calendar = getTodayCalendar()
    calendar.setTime(currentDate)
    calendar.add(Calendar.DATE, 1);  // number of days to add
    calendar.getTime
  }

  def getNextMonth(currentDate: Date): Date = {
    val calendar = getTodayCalendar()
    calendar.setTime(currentDate)
    calendar.add(Calendar.MONTH, 1);
    calendar.getTime
  }

  def lastDayOfMonth(currentDate: Date): Date = {
    val calendar = getTodayCalendar()
    calendar.setTime(currentDate)
    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
    calendar.getTime
  }

  def yesterday(): Date = {
    import java.util.Calendar
    val calendar = getTodayCalendar()
    calendar.add(Calendar.DATE, -1)
    calendar.getTime
  }

  def toDateCol(date: Date, pattern: String): String = {
    s"to_date('${format(date, pattern)}')"
  }

  def lastNDays(currentDay: Date, nBeforeDay: Int): Date = {
    val calendar = getTodayCalendar()
    calendar.setTime(currentDay)
    calendar.add(Calendar.DATE, -nBeforeDay)
    calendar.getTime
  }

  def today(): Date = {
    val calendar = getTodayCalendar()
    calendar.getTime
  }

  def format(date: Date, pattern: String): String = {
    new SimpleDateFormat(pattern).format(date)
  }

  def formatDate(dateColumn: Column): Column = {
    from_unixtime(dateColumn, DATE_PATTERN)
  }

  def yesterdayCol(datePattern: String): String = {
    TimeUtils.format(yesterday(), datePattern)
  }

  def firstDateOfMonth(date: Date): Date = {
    val calendar = getTodayCalendar()
    calendar.setTime(date)
    clearTime(calendar)
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    calendar.getTime
  }

  def toMonth(date: Date, monthPattern: String = "yyyy-MM"): String = {
    TimeUtils.format(date, monthPattern)
  }

  private def getTodayCalendar(): Calendar = {
    val calendar = Calendar.getInstance
    if (mockToday != null) {
      calendar.setTime(mockToday)
    }
    clearTime(calendar)
    calendar
  }

  /**
   * REMOVE time, keep date
   */
  private def clearTime(calendar: Calendar): Unit = {
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.clear(Calendar.MINUTE);
    calendar.clear(Calendar.SECOND);
    calendar.clear(Calendar.MILLISECOND);
  }


  /**
   * Run in mock time [startDate, endDate)
   */
  def runWithMockTime(startDate: Date, endDate: Date, getNextTime: (Date) => Date, fn: () => Unit): Unit = {
    var currentDate = startDate
    while (currentDate.before(endDate)) {
      try {
        TimeUtils.setToDay(currentDate)
        fn()
      } catch {
        case ex: Throwable => error(s"run::error:: ${format(currentDate, "yyyy-MM-dd")}", ex)
      } finally {
        currentDate = getNextTime(currentDate)
      }
    }
  }
}
