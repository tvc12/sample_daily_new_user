package co.datainsider
package util

import co.datainsider.util.JdbcClient.Record
import org.apache.hadoop.shaded.org.jline.utils.Log.{error, info}

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet, Statement}
import java.util.Properties

object JdbcClient {
  type Record = Seq[Any]
}

trait JdbcClient {
  def getConnection(): Connection

  def executeQuery[T](query: String, values: Any*)(implicit
      converter: ResultSet => T
  ): T

  def execute(query: String, values: Any*): Boolean

  def executeUpdate(query: String, values: Any*): Int

  def executeTransaction(
      queries: Array[String],
      valuesArr: Array[Array[Any]]
  ): Boolean

  def executeBatchUpdate(query: String, records: Seq[Record]): Int

  def executeInsert(query: String, values: Any*): Long
}

abstract class AbstractJdbcClient extends JdbcClient {

  def getConnection(): Connection

  /** *
    *
    * Ex: executeQuery( "Select from Users where id = ?;", 1)
    * Supported Type: Boolean, BigDecimal, Byte, Date, Float, Double, Int, Long, String, Time, Timestamp
    *
    * @param query  Parameterized Query
    * @param values Value to put to parameterized query
    * @return
    */
  def executeQuery[T](query: String, values: Any*)(implicit
      converter: ResultSet => T
  ): T = {
    Using(getConnection()) { conn =>
      {
        Using(conn.prepareStatement(query)) { statement =>
          {
            converter(parameterizeStatement(statement, values).executeQuery())
          }
        }
      }
    }
  }

  /** *
    *
    * Ex: executeUpdate( "Insert INTO Users(?,?)", 1L, "User A")
    * Supported Type: Boolean, BigDecimal, Byte, Date, Float, Double, Int, Long, String, Time, Timestamp
    *
    * @param query  Parameterized Query
    * @param values Value to put to parameterized query
    * @return
    */
  def executeUpdate(query: String, values: Any*): Int = {
    Using(getConnection()) { conn =>
      {
        Using(conn.prepareStatement(query)) { statement =>
          {
            parameterizeStatement(statement, values).executeUpdate()
          }
        }
      }
    }
  }

  /** *
    *
    * Execute list of queries in transaction manner, values of each queries is parameterized
    * Make sure to provide queries and corresponding values in order
    *
    * @param queries list of query to be executed in order
    * @param valuesArr values of each queries also in order
    * @return
    */
  def executeTransaction(
      queries: Array[String],
      valuesArr: Array[Array[Any]]
  ): Boolean = {
    Using(getConnection()) { conn =>
      {
        conn.setAutoCommit(false)
        try {
          for ((query, values) <- queries zip valuesArr) yield {
            Using(conn.prepareStatement(query)) { statement =>
              {
                parameterizeStatement(statement, values).executeUpdate()
              }
            }
          }
          conn.commit()
          true
        } catch {
          case _: Exception =>
            conn.rollback()
            false
        } finally conn.setAutoCommit(true)
      }
    }
  }

  private def parameterizeStatement(
      statement: PreparedStatement,
      values: Seq[Any]
  ): PreparedStatement = {
    var paramIndex = 1
    for (value <- values) {
      value match {
        case value: Boolean               => statement.setBoolean(paramIndex, value)
        case value: java.math.BigDecimal  => statement.setBigDecimal(paramIndex, value)
        case value: java.math.BigInteger  => statement.setLong(paramIndex, value.longValue())
        case value: scala.math.BigDecimal => statement.setBigDecimal(paramIndex, value.bigDecimal)
        case value: scala.math.BigInt     => statement.setLong(paramIndex, value.longValue())
        case value: Byte                  => statement.setByte(paramIndex, value)
        case value: java.sql.Date         => statement.setDate(paramIndex, value)
        case value: java.sql.Time         => statement.setTime(paramIndex, value)
        case value: java.sql.Timestamp    => statement.setTimestamp(paramIndex, value)
        case value: Float                 => statement.setFloat(paramIndex, value)
        case value: Double                => statement.setDouble(paramIndex, value)
        case value: Int                   => statement.setInt(paramIndex, value)
        case value: Long                  => statement.setLong(paramIndex, value)
        case value: String                => statement.setString(paramIndex, value)
        case value: java.sql.Array        => statement.setArray(paramIndex, value)
        case null                         => statement.setObject(paramIndex, value)
        case e: Any => {
          info(s"parameterizeStatement:: ${value.getClass}")
          throw new UnsupportedOperationException(s"can not parameterize value of type: $e ")
        }
      }
      paramIndex += 1
    }
    statement
  }

  def executeBatchUpdate(query: String, records: Seq[Record]): Int = {
    Using(getConnection()) { conn =>
      {
        Using(conn.prepareStatement(query)) { statement =>
          {
            var nValidRecord = 0
            records.foreach(record => {
              try {
                parameterizeStatement(statement, record)
                statement.addBatch()
                nValidRecord += 1
              } catch {
                case e: Throwable =>
                  error(s"error when parameterize value for query: $e")
              }
            })
            if (nValidRecord > 0) statement.executeBatch()
            nValidRecord
          }
        }
      }
    }
  }

  def execute(query: String, values: Any*): Boolean = {
    Using(getConnection()) { conn =>
      {
        Using(conn.prepareStatement(query)) { statement =>
          {
            parameterizeStatement(statement, values).execute()
          }
        }
      }
    }
  }

  def executeInsert(query: String, values: Any*): Long = {
    Using(getConnection()) { conn =>
      {
        Using(conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) { statement =>
          {
            parameterizeStatement(statement, values).executeUpdate()
            val rsKeys = statement.getGeneratedKeys
            if (rsKeys.next())
              rsKeys.getLong(1)
            else -1
          }
        }
      }
    }
  }

}

case class NativeJdbcClient(jdbcUrl: String, properties: Properties) extends AbstractJdbcClient {
  override def getConnection(): Connection =
    DriverManager.getConnection(jdbcUrl, properties)
}
