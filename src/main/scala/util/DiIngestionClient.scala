package co.datainsider
package util

import util.TableType.{Replacing, TableType}

import co.datainsider.ClickhouseConfiguration.CLICKHOUSE_URL
import com.google.gson.{Gson, JsonObject}
import okhttp3._
import org.apache.hadoop.shaded.org.jline.utils.Log.{error, info}
import org.apache.spark.sql.types.StructField

import java.util.Properties
import java.util.concurrent.TimeUnit

object TableType extends Enumeration {
  type TableType = Value
  val Default: TableType = Value("default")
  // table with logic replace data when two row same sort key value
  val Replacing: TableType = Value("replacing")
}/**
  * @author tvc12 - Thien Vi
  * @created 12/06/2021 - 5:48 PM
  */

case class DiIngestionClient(host: String, serviceKey: String, timeoutInSec: Int, clickhouseUrl: String, properties: Properties) {


  private val client = new OkHttpClient().newBuilder()
    .connectTimeout(timeoutInSec, TimeUnit.SECONDS)
    .writeTimeout(timeoutInSec, TimeUnit.SECONDS)
    .readTimeout(timeoutInSec, TimeUnit.SECONDS)
    .build()

  private def toTableSchemaJson(dbName: String, tblName: String, columns: Array[StructField], isOverride: Boolean, tableType: TableType, primaryKeys: Array[String]): String = {
    val jsonObject: JsonObject = new JsonObject
    val gson = new Gson
    val columnsAsJsonObject = toJsonColumns(tableType: TableType, columns, primaryKeys)

    jsonObject.addProperty("organization_id", 0L) // TODO: FIXED ORG
    jsonObject.addProperty("db_name", dbName)
    jsonObject.addProperty("tbl_name", tblName)
    val writeMode = if (isOverride) "overwrite" else "append"
    jsonObject.addProperty("write_mode", writeMode)
    jsonObject.add("columns", gson.toJsonTree(columnsAsJsonObject))
    jsonObject.addProperty("table_type", tableType.toString)
    jsonObject.add("order_bys", gson.toJsonTree(primaryKeys))
    if (tableType == TableType.Replacing) {
      jsonObject.add("primary_keys", gson.toJsonTree(primaryKeys))
    }
    jsonObject.toString
  }

  def ensureTableSchema(dbName: String,
                        tblName: String,
                        columns: Array[StructField],
                        isOverride: Boolean,
                        tableType: TableType,
                        primaryKeys: Array[String]
                       ): Boolean = {
    val tableSchemaAsJson: String = toTableSchemaJson(dbName, tblName, columns, isOverride, tableType, primaryKeys)
    info(s"ensureTableSchema::tableSchemaAsJson ${tableSchemaAsJson}")
    val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), tableSchemaAsJson)
    val httpRequest = new Request.Builder()
      .url(s"${host}/ensure_schema")
      .header("DI-SERVICE-KEY", serviceKey)
      .post(body)
      .build
    val response = client.newCall(httpRequest).execute
    if (response.isSuccessful) {
      true
    } else {
      throw new Exception(s"Error to ingest data to DataInsider: ${response}")
    }
  }

  private def toJsonColumns(tableType: TableType, columns: Array[StructField], primaryKeys: Array[String]): Array[JsonObject] = {
    val setPrimaryKeys = primaryKeys.toSet
    columns.map(colum => {
      val json = new JsonObject
      json.addProperty("name", colum.name)
      json.addProperty("data_type", colum.dataType.toString)
      json.addProperty("default_value", "")
      if (tableType == TableType.Replacing && setPrimaryKeys.contains(colum.name)) {
        json.addProperty("is_nullable", false)
      } else {
        json.addProperty("is_nullable", true)
      }

      json
    })
  }

  def toOptimizeRequestAsJson(dbName: String, tblName: String, primaryKeys: Array[String]): String = {
    val jsonObject: JsonObject = new JsonObject
    val gson = new Gson

    jsonObject.addProperty("organization_id", 0L) // TODO: FIXED ORG
    jsonObject.addProperty("db_name", dbName)
    jsonObject.addProperty("tbl_name", tblName)
    jsonObject.add("primary_keys", gson.toJsonTree(primaryKeys))
    jsonObject.addProperty("is_use_final", true)
    jsonObject.toString
  }

  /**
   * Method for test deduplicate data, will replace with api
   */
  def deduplicate(dbName: String, tblName: String, primaryKeys: Array[String]): Boolean = {
    val optimizeRequestAsJson = toOptimizeRequestAsJson(dbName, tblName, primaryKeys)
    val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), optimizeRequestAsJson)
    val httpRequest = new Request.Builder()
      .url(s"${host}/optimize_table")
      .header("DI-SERVICE-KEY", serviceKey)
      .post(body)
      .build
    val response = client.newCall(httpRequest).execute
    if (!response.isSuccessful) {
      error(s"deduplicate::error ${response.message()}")
    }
    response.isSuccessful
  }
}
