package co.datainsider
package util

import co.datainsider.ClickhouseConfiguration.{CLICKHOUSE_URL, DI_HOST, JDBC_PROPERTIES, SECRET_KEY, TIMEOUT_IN_SECOND}
import TableType.TableType
import org.apache.hadoop.shaded.org.jline.utils.Log.error
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.util.Date

/**
  * @author tvc12 - Thien Vi
  * @created 12/14/2021 - 4:45 PM
  */
object Implicits {
  private val client = DiIngestionClient(DI_HOST, SECRET_KEY, TIMEOUT_IN_SECOND, CLICKHOUSE_URL, JDBC_PROPERTIES)

  implicit class ImplicitDataFrame(val df: DataFrame) extends AnyVal {
    def saveToHadoop(path: String, saveMode: SaveMode = SaveMode.Overwrite): Unit = {
      if (saveMode == SaveMode.Overwrite && HadoopUtils.isPathExists(df.sparkSession.sparkContext, path)) {
        HadoopUtils.moveToTrash(df.sparkSession.sparkContext, path)
      }
      df.write.mode(saveMode).parquet(path)
    }

    def saveToHadoopWithPartition(baseDir: String, partitionDate: Date, fileName: String, saveMode: SaveMode = SaveMode.Overwrite): Unit = {
      val path = PathUtils.buildPath(baseDir, partitionDate, fileName)
      saveToHadoop(path, saveMode)
    }

    /**
     * allow force deduplicate with primary keys not empty
     * Skip deduplicate if primary keys is empty
     */
    def saveToClickHouse(dbName: String, tblName: String, saveMode: SaveMode, tableType: TableType = TableType.Default, primaryKeys: Array[String] = Array.empty, forceDeduplicate: Boolean = false): Unit = {
      try {
        client.ensureTableSchema(dbName, tblName, df.schema.fields, saveMode == SaveMode.Overwrite, tableType, primaryKeys)
        df.write.mode(SaveMode.Append).jdbc(s"$CLICKHOUSE_URL/${dbName}", tblName, JDBC_PROPERTIES)
      } catch {
        case ex: Throwable => error("saveToClickHouse::error", ex)
      } finally {
        if (forceDeduplicate) {
          client.deduplicate(dbName, tblName, primaryKeys)
        }
      }
    }
  }

  implicit class ImplicitOptionDataFrame(val df: Option[DataFrame]) extends AnyVal {
    def saveToHadoop(path: String, saveMode: SaveMode = SaveMode.Overwrite): Unit = {
      df.foreach(_.saveToHadoop(path, saveMode))
    }

    def saveToClickHouse(dbName: String, tblName: String, saveMode: SaveMode, tableType: TableType = TableType.Default, primaryKeys: Array[String] = Array.empty): Unit = {
      df.foreach(_.saveToClickHouse(dbName, tblName, saveMode, tableType, primaryKeys))
    }
  }

  implicit class ImplicitSparkSession(val sparkSession: SparkSession) extends AnyVal {
    // create view with path & view name
    def createView(parquetPath: String, viewName: String): Unit = {
      sparkSession.read.parquet(parquetPath).createOrReplaceTempView(viewName)
    }

    // create view using partition date
    def createView(baseDir: String, partitionDate: Date, parquetFileName: String, viewName: String): Unit = {
      val path: String = PathUtils.buildPath(baseDir, partitionDate, parquetFileName)
      createView(path, viewName)
    }

    // create view using start date & end date
    def createView(baseDir: String, startDate: Date, endDate: Date, parquetFileName: String, viewName: String): Unit = {
      val paths: Array[String] = PathUtils.buildPaths(baseDir, startDate, endDate, parquetFileName)
      createView(paths, viewName)
    }

    // create view with list parquet paths
    def createView(parquetPaths: Array[String], viewName: String): Unit = {
      val existPaths = parquetPaths.filter(HadoopUtils.isPathExists(sparkSession.sparkContext, _))
      sparkSession.read.parquet(existPaths: _*).createOrReplaceTempView(viewName)
    }
  }
}
