ThisBuild / version := "1.0.0"

ThisBuild / scalaVersion := "2.12.12"

lazy val root = (project in file("."))
  .settings(
    name := "sample-daily-new-user-analysis",
    idePackagePrefix := Some("co.datainsider")
  )

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "3.2.0",
  "org.apache.spark" %% "spark-core" % "3.2.0",
  // https://mvnrepository.com/artifact/com.github.housepower/clickhouse-native-jdbc
   "com.github.housepower" % "clickhouse-native-jdbc" % "2.4.4",
  // https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp
  "com.squareup.okhttp3" % "okhttp" % "4.9.0"
)

assemblyJarName in assembly := "sample-daily-new-user-analysis-1.0.0.jar"
// META-INF discarding
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
