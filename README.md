### BUILD

```sh
sbt assembly
```

#### RUN

```sh
$SPARK_HOME/bin/spark-submit \
sample-daily-new-user-analysis-1.0.0.jar
```
